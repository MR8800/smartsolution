import React from 'react';
import Navigation from './Navigation/Navigation'
import { ContextProvider } from "./contexts/Context"

export default class App extends React.Component {
  render() {
    return (
      <ContextProvider>
        <Navigation/>
      </ContextProvider>
    )
  }
}