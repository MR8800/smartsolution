import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import LoginScreen from '../Screens/LoginScreen'
import HomeScreen from '../Screens/HomeScreen'
import RegisterScreen from '../Screens/RegisterScreen'
import ForgotPasswordMailScreen from "../Screens/ForgotPasswordMailScreen";
import ForgotPasswordCodeScreen from "../Screens/ForgotPasswordCodeScreen";
import NewPasswordScreen from "../Screens/NewPasswordScreen"
import WelcomeScreen from "../Screens/WelcomeScreen";


const AppStackNavigator = createStackNavigator({
  LoginScreen: {
    screen: LoginScreen,
    navigationOptions: {
      title: 'Login'
    }
  },
  HomeScreen: {
    screen: HomeScreen,
    navigationOptions: {
      title: 'Accueil'
    }
  },
  RegisterScreen: {
    screen: RegisterScreen,
    navigationOptions: {
      title: 'Inscription'
    }
  },
  ForgotPasswordMailScreen: {
    screen: ForgotPasswordMailScreen,
    navigationOptions: {
      title: 'Mot de passe oublié'
    }
  },
  ForgotPasswordCodeScreen: {
    screen: ForgotPasswordCodeScreen,
    navigationOptions: {
      title: 'Mot de passe oublié'
    }
  },
  NewPasswordScreen: {
    screen: NewPasswordScreen,
    navigationOptions: {
      title: 'Mot de passe oublié'
    }
  },
  WelcomeScreen: {
    screen: WelcomeScreen,
    navigationOptions: {
      title: 'Bienvenue',
      headerLeft: () => {
        return null
      }
    }
  }  
})

export default createAppContainer(AppStackNavigator)