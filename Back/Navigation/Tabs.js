import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeScreen from '../Screens/HomeScreen';
import SettingScreen from '../Screens/SettingScreen';
import ProfileScreen from '../Screens/ProfileScreen';
import { NavigationContainer } from '@react-navigation/native';
import React from 'react';


const Tab = createBottomTabNavigator();

class Tabs extends React.Component {
    render() {
        return (
            <Tab.Navigator>
               <Tab.Screen name= "Home" component={HomeScreen}/>
               <Tab.Screen name= "Settings" component={SettingScreen}/>
               <Tab.Screen name= "Profile" component={ProfileScreen}/>
            </Tab.Navigator>

        )
    }
}

export default Tabs;