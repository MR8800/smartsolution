import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
} from "react-native";

class ForgotPasswordCodeScreen extends React.Component {
    render() {
        return (
          <View style={styles.container}>
     
          <StatusBar style="auto" />
          <View style={styles.inputView}>
            <TextInput
              style={styles.TextInput}
              placeholder="Entrez votre code."
              placeholderTextColor="#000000"
            />
          </View>

  
          <TouchableOpacity style={styles.loginBtn} onPress={()=>this.props.navigation.navigate("NewPasswordScreen")}>
            <Text style={styles.loginText}>VALIDER</Text>
          </TouchableOpacity>
      </View>
    );
    }
}
  
     
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        backgroundColor: "#FFFFFF",
        alignItems: "center",
        justifyContent: "center",
      },
     
      image: {
        resizeMode: "contain",
        width: 200,
        height: 150,
      },
     
      inputView: {
        borderRadius: 30,
        width: "70%",
        height: 45,
        marginBottom: 20,
     
        alignItems: "center",
    
        borderWidth: 1,
        overflow: 'hidden',
        shadowColor: '#DCDCDC',
        shadowRadius: 10,
        shadowOpacity: 1,
    
      },
  
      checkboxView: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center"
      },
     
      TextInput: {
        height: 50,
        flex: 1,
        padding: 10,
        marginLeft: 20,
      },
     
      forgotButton: {
        height: 30,
        marginBottom: 30,
      },
     
      loginBtn: {
        width: "80%",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 40,
        backgroundColor: "#f00020",
      },
  
      loginBtnDisable: {
        width: "80%",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 40,
        backgroundColor: "gray"
      },
  
      loginText: {
        color: "white"
      },
  
      loginTextDiable: {
        color: "white"
      }
    });

export default ForgotPasswordCodeScreen;  