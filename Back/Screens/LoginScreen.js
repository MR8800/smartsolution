import { StatusBar } from "expo-status-bar";
import React, { useState, useCallback } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
} from "react-native";
import { DispatchContext, StateContext } from "../contexts/Context";
import { useLogin } from "../hooks/useLogin";
import ForgotPasswordMailScreen from "./ForgotPasswordMailScreen";

const isEmpty = (obj) => {
  
}

const LoginScreen = ({navigation}) => {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const { user } = React.useContext(StateContext)
  const dispatch = React.useContext(DispatchContext)
  const { data, error, login } = useLogin();

  const onChangeEmail = useCallback((ev) => {
    setEmail(ev.target.value)
  }, [])

  const onChangePassword = useCallback((ev) => {
    setPassword(ev.target.value)
  }, [])

  const onLogin = useCallback(() => {
    login({
      email,
      password
    })
  }, [login, email, password])

  React.useEffect(() => {
    console.log("data: ", data)
    if (data?.token) {
      localStorage.setItem("auth_user", JSON.stringify(data))
      dispatch({type: "SET_AUTH", payload: data});
      navigation.navigate("WelcomeScreen")
    }
  }, [data, dispatch, navigation]);

  const onRegister = useCallback(() => {
    navigation.navigate("RegisterScreen")
  }, [navigation])

  const onForgot = useCallback(() => {
    navigation.navigate("ForgotPasswordMailScreen")
  }, [navigation])

//  if (JSON.parse(localStorage.getItem("auth_user"))) {
//  navigation.navigate("WelcomeScreen")
//  }

  React.useEffect(() => {
    console.log("user: ", user)
    if (user?.token) {
      console.log("++111")
      navigation.navigate("WelcomeScreen")
    }
  }, [user, navigation])

      return (
        <View style={styles.container}>
        <Image style={styles.image} source={require("../assets/SmartLogo.png")}/>
        <StatusBar style="auto" />
        <View style={styles.inputView}>
          <TextInput
            style={styles.TextInput}
            placeholder="Email."
            placeholderTextColor="#000000"
            value={email}
            onChange={onChangeEmail}
          />
        </View>
   
        <View style={styles.inputView}>
          <TextInput
            style={styles.TextInput}
            placeholder="Mot de passe."
            placeholderTextColor="#000000"
            secureTextEntry={true}
            value={password}
            onChange={onChangePassword}
          />
        </View>

   
        <TouchableOpacity >
          <Text style={styles.forgotButton} onClick={onForgot}>Mot de passe oublié?</Text>
        </TouchableOpacity>

        <TouchableOpacity>
          <Text style={styles.forgotButton} onClick={onRegister}>Créer un compte</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.loginBtn} onPress={onLogin} >
          <Text style={styles.loginText}>LOGIN</Text>
        </TouchableOpacity>
    </View>
  );
}

   
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#FFFFFF",
      alignItems: "center",
      justifyContent: "center",
    },
   
    image: {
      resizeMode: "contain",
      width: 200,
      height: 150,
    },
   
    inputView: {
      borderRadius: 30,
      width: "70%",
      height: 45,
      marginBottom: 20,
   
      alignItems: "center",
  
      borderWidth: 1,
      overflow: 'hidden',
      shadowColor: '#DCDCDC',
      shadowRadius: 10,
      shadowOpacity: 1,
  
    },

    checkboxView: {
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "center"
    },
   
    TextInput: {
      height: 50,
      flex: 1,
      padding: 10,
      marginLeft: 20,
    },
   
    forgotButton: {
      height: 30,
      marginBottom: 30,
    },
   
    loginBtn: {
      width: "80%",
      borderRadius: 25,
      height: 50,
      alignItems: "center",
      justifyContent: "center",
      marginTop: 40,
      backgroundColor: "#f00020",
    },

    loginBtnDisable: {
      width: "80%",
      borderRadius: 25,
      height: 50,
      alignItems: "center",
      justifyContent: "center",
      marginTop: 40,
      backgroundColor: "gray"
    },

    loginText: {
      color: "white"
    },

    loginTextDiable: {
      color: "white"
    }
  });

  export default LoginScreen;