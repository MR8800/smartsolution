import React from 'react';
import { StyleSheet, View, ScrollView, Text } from 'react-native';



const ProfileScreen = () => {
  const [user, setUser] = React.useState(JSON.parse(localStorage.getItem("auth_user")))
  return (
    <View>
      <Text>{user.email}</Text>
      <Text>{user.username}</Text>
    </View>
  )
}

export default ProfileScreen;