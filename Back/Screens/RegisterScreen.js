import { StatusBar } from "expo-status-bar";
import React, { useState, useCallback } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
} from "react-native";
import { useRegister } from "../hooks/useRegister";

const RegisterScreen = ({navigation}) => {

  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { data, error, register } = useRegister();

  const onChangeUsername = useCallback((ev) => {
    setUsername(ev.target.value)
  })
  const onChangeEmail = useCallback((ev) => {
    setEmail(ev.target.value)
  })
  const onChangePassword = useCallback((ev) => {
    setPassword(ev.target.value)
  })

  const onRegister = useCallback(() => {
    if (!username) {
      alert("input username")
      return;
    }
    if (!email) {
      alert("input email")
      return;
    }
    if (!password) {
      alert("input password")
      return;
    }
    console.log("register")
    register({
      username,
      email,
      password
    })
  }, [register, username, email, password])

  React.useEffect(() => {
    if (data) {
      navigation.navigate("LoginScreen")
    }
  }, [data])

    return (
      <View style={styles.container}>
        <Image style={styles.image} source={require("../assets/SmartLogo.png")} />
        <StatusBar style="auto" />
        <View style={styles.inputView}>
          <TextInput
            style={styles.TextInput}
            placeholder="Username"
            placeholderTextColor="#000000"
            value={username}
            onChange={onChangeUsername}
          />
        </View>

        <View style={styles.inputView}>
          <TextInput
            style={styles.TextInput}
            placeholder="Email"
            placeholderTextColor="#000000"
            value={email}
            onChange={onChangeEmail}
          />
        </View>

        <View style={styles.inputView}>
          <TextInput
            style={styles.TextInput}
            placeholder="password"
            placeholderTextColor="#000000"
            secureTextEntry={true}
            value={password}
            onChange={onChangePassword}
          />
        </View>


        <TouchableOpacity style={styles.loginBtn} onPress={onRegister}>
          <Text style={styles.loginText}>SIGNUP</Text>
        </TouchableOpacity>
      </View>
    );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    alignItems: "center",
    justifyContent: "center",
  },

  image: {
    resizeMode: "contain",
    width: 200,
    height: 150,
  },

  inputView: {
    borderRadius: 30,
    width: "70%",
    height: 45,
    marginBottom: 20,

    alignItems: "center",

    borderWidth: 1,
    overflow: 'hidden',
    shadowColor: '#DCDCDC',
    shadowRadius: 10,
    shadowOpacity: 1,

  },

  checkboxView: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },

  TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 20,
  },

  forgotButton: {
    height: 30,
    marginBottom: 30,
  },

  loginBtn: {
    width: "80%",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    backgroundColor: "#f00020",
  },

  loginBtnDisable: {
    width: "80%",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    backgroundColor: "gray"
  },

  loginText: {
    color: "white"
  },

  loginTextDiable: {
    color: "white"
  }
});

export default RegisterScreen;  