import React from 'react';
import { StyleSheet, View, ScrollView, Text, TouchableOpacity } from 'react-native';
import LoginScreen from "./LoginScreen";
import { StateContext, DispatchContext } from '../contexts/Context';


const SettingScreen = ({ navigation }) => {
  const dispatch = React.useContext(DispatchContext)
  const onLogout = React.useCallback(() => {
    dispatch({ type: "PURGE_AUTO" })
    navigation.navigate("LoginScreen")
    localStorage.clear()
    location.reload()
  }, [dispatch])
  return (
    <View>
      <TouchableOpacity style={styles.loginBtn} onPress={onLogout}>
        <Text style={styles.loginText}>LOGOUT</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.loginBtn}>
        <Text style={styles.loginText}>DELETE ACCOUNT</Text>
      </TouchableOpacity>
    </View>


  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    alignItems: "center",
    justifyContent: "center",
  },

  image: {
    resizeMode: "contain",
    width: 200,
    height: 150,
  },

  inputView: {
    borderRadius: 30,
    width: "70%",
    height: 45,
    marginBottom: 20,

    alignItems: "center",

    borderWidth: 1,
    overflow: 'hidden',
    shadowColor: '#DCDCDC',
    shadowRadius: 10,
    shadowOpacity: 1,

  },

  checkboxView: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },

  TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 20,
  },

  forgotButton: {
    height: 30,
    marginBottom: 30,
  },

  loginBtn: {
    width: "80%",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    backgroundColor: "#f00020",
  },

  loginBtnDisable: {
    width: "80%",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    backgroundColor: "gray"
  },

  loginText: {
    color: "white"
  },

  loginTextDiable: {
    color: "white"
  }
});


export default SettingScreen;