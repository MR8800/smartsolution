import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import Tabs from '../Navigation/Tabs';
import React from 'react';



class WelcomeScreen extends React.Component {
    render() {
      return (
          <NavigationContainer>
              <Tabs/>
          </NavigationContainer>
      )
    }
}

export default WelcomeScreen;