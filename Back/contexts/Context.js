import React from "react";

const INITIAL_STATE = {
    isAuthenticated: false,
    user: {}
}

const StateContext = React.createContext({});
const DispatchContext = React.createContext({});

const reducer = (state, action) => {
    switch (action.type) {
        case "SET_AUTH":
            return {...state, isAuthenticated: true, user: action.payload };
        case "PURGE_AUTH":
            return {...state, isAuthenticated: false, user: null };
    
        default:
            return state;
    }
}

const ContextProvider = ({ children }) => {
    const [state, dispatch] = React.useReducer(reducer, INITIAL_STATE);

    return (
        <StateContext.Provider value={state}>
            <DispatchContext.Provider value={dispatch}>
                {children}
            </DispatchContext.Provider>
        </StateContext.Provider>
    )
}

export { StateContext, DispatchContext, ContextProvider };