import React from "react";

import { Api } from "../utils/api"

export const useLogin = () => {
    const [data, setData] = React.useState();
    const [error, setError] = React.useState();

    const login = React.useCallback(async ({email, password}) => {
        try {
            const { data } = await Api.post("/login", {email, password})
            console.log("loginData: ", data)
            setError(null)
            setData(data)
        } catch (err) {
            setError(err);
            setData(null);
        }
    }, [])

    return {data, error, login}
}
