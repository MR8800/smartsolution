import React from "react";

import { Api } from "../utils/api"

export const useRegister = () => {
    const [data, setData] = React.useState();
    const [error, setError] = React.useState();

    const register = React.useCallback(async ({username, email, password}) => {
        console.log("username, e, pas", username, email, password)
        try {
            const { data } = await Api.post("/register", {username, email, password});
            console.log("registerdata1: ", data)
            if (data.status) {
                setData(data);
                setError(null)
                alert(data.message)
            }

        } catch (err) {
            setError(err);
            setData(null);
        }
    }, []);

    return { data, error, register }
}