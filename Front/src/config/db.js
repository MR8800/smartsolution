const mysql = require('mysql2');
require("dotenv").config();

const db_connection = mysql.createConnection({
    host: process.env.MYSQL_HOST,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_ROOT_PASSWORD,
    database: process.env.MYSQL_DATABASE,
    dateStrings: true
});

db_connection.connect(function(err) {
    if (err) {
        throw(err);
        console.log(err);
    } else {
        console.log("Connected to database");
    }
});

module.exports = db_connection;
