const express = require("express");
var path = require ('path');
const cors = require("cors")
var bodyParser = require("body-parser");
const app = express();
const port = 3000;
const login = require("./routes/auth/login");
const register = require("./routes/auth/register");
const { pool } = require ("./config/db");
const accueil = require("./routes/accueil/accueil");
const  wesh = require('./routes/middleware/middleware');
const controller = require("./routes/auth/confirm")

app.use(cors())

app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname + '/index.html'))
});

/* Login et Register */
app.post("/register", register);
app.post ("/login", login);
app.get("/confirm/:confirmationCode", controller.verifyUser);

/* Page d'accueil */
app.get("/accueil/:accesstoken", accueil);

app.listen(port, function () {
    console.log('Example app listening on port ' +port)
});
