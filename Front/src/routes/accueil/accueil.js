const wesh = require('../middleware/middleware')
const router = require('express').Router();
var bodyParser = require("body-parser");
var path = require ('path');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.get("/accueil/:accesstoken", wesh,(req, res) => {
    res.sendFile(path.join(__dirname + '/accueil.html'))
})

module.exports = router;