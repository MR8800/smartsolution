const jwt = require("jsonwebtoken")
const pool = require("../../config/db")
require('dotenv').config();

exports.verifyUser = (req, res, next) => {
    const decode = jwt.verify(req.params.confirmationCode, process.env.SECRET);
    const mail = decode.userMail
    console.log(mail)
    pool.query ('SELECT * FROM user WHERE email = ?', mail, function(err, result) {
        if (err) {
            res.send("cant match email")
        }
        var ThisUSER = {
            id : result[0].id,
            email : result[0].email,
            password : result[0].password,
            username : result[0].username,
            created_at : result[0].created_at,
            status : "active",
            confirmationCode : result[0].confirmationCode
        };
        pool.query('UPDATE user SET ? WHERE email = ?', [ThisUSER, mail], function(err1) {
            if (err1) throw(err1);
            res.redirect('/');
        });
    });
};