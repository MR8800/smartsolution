const router = require('express').Router();
const pool = require ("../../config/db");
const accueil = require("../accueil/accueil");
const bcrypt = require ("bcrypt");
const jwt = require('jsonwebtoken');
const cookieParser = require('cookie-parser');
const middleware = require('../middleware/middleware')
var bodyParser = require("body-parser");
var path = require ('path');
var newUser;

require("dotenv").config();

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.get("/login", (req, res) => {
    res.sendFile(path.join(__dirname + '/login.html'))
});

router.post("/login", (req, res) => {
    console.log("login: ", req.body)
    pool.query ('SELECT email FROM user WHERE email = ?', req.body.email, function(error, result) {
        if(!(result.length === 0)) {
            console.log("email Exist")
            pool.query ('SELECT * FROM user WHERE email = ?', req.body.email, function(error, result1) {
                console.log("22", result1[0].password)
                const log = (result1[0].password);
                bcrypt.compare(req.body.password, log, function(err, reslt) {
                    const useremail = (result1[0].email);
                    if (reslt == true) {
                        console.log("ss", result1[0].status)
                        if (result1[0].status != "active") {
                            return res.status(401).send({
                            message: "Pending Account. Please Verify Your Email!",
                            });
                        }
                        const accesstok = jwt.sign({ useremail }, process.env.SECRET)
                        console.log("token: ", accesstok)
                        req.cookie = accesstok;
                        res.status(200).send({
                            token: accesstok,
                            username: result1[0].username,
                            email: result1[0].email
                        })
                    }
                    else
                        res.send("Wrong password")
                });
            });
        }
        else
            res.send("cant match email")
    });
});

module.exports = router;