const router = require('express').Router();
const pool = require ("../../config/db");
const jwt = require("jsonwebtoken");
const bcrypt = require ("bcrypt");
const nodemailer = require("./mailconfirm")
var bodyParser = require("body-parser");
var path = require ('path');
require("dotenv").config();
var newUser;

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.get("/register", (req, res) => {
    res.sendFile(path.join(__dirname + '/register.html'))
})

router.post("/register", (req, res) => {
    pool.query ('SELECT email FROM user WHERE email = ?', req.body.email, function(error, result) {
        if (error) throw(error);
        if (result.length > 0) {
            console.log(result);
            res.status(200).send("Account already exists || email already used");
        }
    });
    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(req.body.password, salt);
    const userMail = req.body.email;
    newUser = {
        email: req.body.email,
        password: hash,
        username: req.body.username,
        confirmationCode: jwt.sign({ userMail }, process.env.SECRET)
    };
    console.log(newUser.confirmationCode);
    pool.query('INSERT INTO user SET ?', newUser, function(error) {
        if (error)throw(error);
        else {
            nodemailer.sendConfirmationEmail(
                newUser.username,
                newUser.email,
                newUser.confirmationCode
            );
            res.send({
                status: true,
                message: "Account created. Check your email to verify your account"
            })
        }
    });
});

module.exports = router;