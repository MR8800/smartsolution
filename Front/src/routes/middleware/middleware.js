const jwt = require("jsonwebtoken");
var bodyParser = require("body-parser");
require("dotenv").config();

module.exports = (req, res, next) => {
    try {
        jwt.verify(req.params.accesstoken, process.env.SECRET, (err) => {
            if (err) {
                res.status(418).send('"msg" : "Token is not valid')
            } else {
                next();
            }
        });
    }catch{
        res.status(417).send('"msg": "No token, authorization denied"')
    }
};