DROP DATABASE IF EXISTS `template`;
CREATE DATABASE IF NOT EXISTS `template`;
USE `template`;

CREATE TABLE IF NOT EXISTS `user` (
       `id` int(11) NOT NULL AUTO_INCREMENT,
       `email` varchar(255) NOT NULL,
       `password`varchar(255) NOT NULL,
       `name` varchar(255) NOT NULL,
       `firstname` varchar(255) NOT NULL,
       `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
       `status` ENUM('pending','active') default 'pending',
       `confirmationCode` varchar(255) NOT NULL,
       PRIMARY KEY (id)
);
